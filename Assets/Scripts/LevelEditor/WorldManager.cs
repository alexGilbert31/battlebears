﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class WorldManager : MonoBehaviour {


	//public ObjectData[] exempleDebugJson; // To use in the editor

	public string GetData () {
		LevelData data = new LevelData ();

		LevelObject[] objectsInScene = FindObjectsOfType<LevelObject> ();

		foreach (LevelObject obj in objectsInScene) {
			//We create a new Dataobject for all the levelObject, and we store the necessary information in there
			ObjectData objData = new ObjectData ();
			objData.name = obj.name;
			objData.position = obj.transform.position;
			objData.rotation = obj.transform.rotation;
			objData.scale = obj.transform.localScale;

			//In case of the spawner object, we also need to store the team of the spawner
			Spawner s = obj.GetComponent<Spawner> ();
			if (s != null) {
				objData.team = s.team;
			}

			data.objectList.Add (objData);
		}

		//False = pretty print, smaller size if it's not to true
		string jsonString = JsonUtility.ToJson (data, false);
	
		return jsonString;
	}

	public void SetData (string jsonString) {
		//Delete old level
		foreach (LevelObject obj in FindObjectsOfType<LevelObject>()) {
			Undo.DestroyObjectImmediate (obj.gameObject);
		}

		//get JSON data
		LevelData data = JsonUtility.FromJson<LevelData> (jsonString);

		//Create new level form JSON
		foreach (ObjectData objData in data.objectList) {
			
			//Search inside the resources folder for the correct name inside the JSON to get the prefab
			LevelObject objPrefab = Resources.Load<LevelObject> (objData.name);

			//Error Handling if name is incorrect and trow error
			if (objPrefab == null) {
				//Debug.LogError is heavy on performance but very very useful
				Debug.LogError (objData.name + " does not exist inside the Resources folder");
				continue;
			}

			LevelObject objClone = Instantiate (objPrefab);

			//Created a CMD + Z undo
			Undo.RegisterCreatedObjectUndo (objClone.gameObject, "Created Level");


			//Unity always put Clone after the clone object, we dont want that so we set/force the name
			objClone.name = objData.name;
			objClone.transform.position = objData.position;
			objClone.transform.rotation = objData.rotation;
			objClone.transform.localScale = objData.scale;

			//Set the team for the spawner
			Spawner s = objClone.GetComponent<Spawner> ();
			if (s != null) {
				s.team = objData.team;
			}

		}
	}
}
