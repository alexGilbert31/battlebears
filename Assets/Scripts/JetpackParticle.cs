﻿using UnityEngine;
using System.Collections;

public class JetpackParticle : MonoBehaviour {

	public ParticleSystem[] jet;
	// Use this for initialization
	void Start () {
		jet = GetComponentsInChildren<ParticleSystem> ();
	}

	public void StopJet () {
		foreach (ParticleSystem part in jet) {
			part.Stop ();
		}
	}

	public void StartJet () {
		foreach (ParticleSystem part in jet) {
			part.Play ();
		}
	}
}
