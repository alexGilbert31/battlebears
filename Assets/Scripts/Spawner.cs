﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public AIController AIPrefab;
	public AIController AIPrefabWithJetpack;
	public float spawnInterval;
	public int team;
	public int percentageJetPackSpawner = 100;

	// Use this for initialization
	IEnumerator Start () {
		//Wait one frame for the game scene to be active
		yield return null;
		while (true) {
			AIController AIClone;
			int rand = Random.Range (0, 99);

			if (rand < percentageJetPackSpawner) {

				AIClone = (AIController)Instantiate (AIPrefabWithJetpack, transform.position, transform.rotation);
				AIClone.team = team;

			} else {

				AIClone = (AIController)Instantiate (AIPrefab, transform.position, transform.rotation);
				AIClone.team = team;
			
			}

		
			yield return new WaitForSeconds (spawnInterval);
		}


	}
	

}
