﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class UIManager : MonoBehaviour {

	private static UIManager _instance;

	public static UIManager instance {
		get { 
			if (_instance == null) {
				_instance = FindObjectOfType<UIManager> ();
			}

			return _instance;
		}
	}

	//We create a dictionary so that the Type of any screen can be store as the key and we can access the instance in the propreties
	private Dictionary<Type, UIScreen> screens;

	public UIScreen openingScreen;

	private UIScreen currentScreen;


	// Use this for initialization
	void Awake () {
		screens = new Dictionary<Type, UIScreen> ();

		foreach (UIScreen screen in GetComponentsInChildren<UIScreen> ()) {
			//We deactivate every screen at the start
			screen.gameObject.SetActive (false);

			//Store all scren inside the dictionary
			screens.Add (screen.GetType (), screen);
		}

		//get the type of the openingScreen that has been set in the inspector
		//null check to make sure the openingScreen
		if (openingScreen != null) {
			Show (openingScreen.GetType ());
		} else {
			Debug.LogWarning ("Set your opening screen inside the unity editor under the UIManager script");
		}
	}
		
	//Generic method with a constraint (where...), that allow to pass only a Type that inherit from UIScreen
	public void Show<T> () where T : UIScreen {
		Type screenType = typeof(T);
		Show (screenType);
	}

	private void Show (Type screenType) {
		if (currentScreen != null) {
			currentScreen.gameObject.SetActive (false);
		}

		//We acces the screens dictionary by providing the type as a key, wich will return the instance of that type
		UIScreen newScreen = screens [screenType];

		newScreen.gameObject.SetActive (true);
		currentScreen = newScreen;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
