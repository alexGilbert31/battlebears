﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;


public class LevelEditorWindow : EditorWindow {

	private string levelName;
	private WorldManager world;

	private const string PATH_LEVELS = "Assets/Levels/";

	[MenuItem ("Tools/PG08 Level Editor")]
	public static void ShowWindow () {
		//This will show a new
		EditorWindow.GetWindow<LevelEditorWindow> ();
	}

	void OnGUI () {

		if (world == null) {
			world = FindObjectOfType<WorldManager> ();
			//If there was no world, we shouldn't allow the user to save
			if (world == null) {
				GUILayout.Label ("No world found in the scene");
				return;
			}
		}

		levelName = EditorGUILayout.TextField ("Name of level: ", levelName);

		if (GUILayout.Button ("Save")) {
			string filePath = PATH_LEVELS + levelName + ".json";
			//We create a file 
			StreamWriter sw = File.CreateText (filePath);

			//We place the JSON data in the file
			sw.Write (world.GetData ());

			//IMPORTANT TO CLOSE the streamwriter, after using it
			sw.Close ();


			//Refresh the project view so we can see the saved files
			AssetDatabase.Refresh ();
		}

		GUILayout.Label ("Click on the level to load it");
		DirectoryInfo dir = new DirectoryInfo (PATH_LEVELS);
		//This return an array of all files in this directory (= my Levels)
		FileInfo[] files = dir.GetFiles ("*.json");

		foreach (FileInfo file in files) {

			if (GUILayout.Button (file.Name.Replace (".json", ""))) {
				

				StreamReader sr = file.OpenText ();
				string jsonString = sr.ReadToEnd ();
				//IMPORTANT TO CLOSE the streamReader, after using it
				sr.Close ();


				world.SetData (jsonString);

				Debug.LogWarning ("Don't forgert to rebake the navmesh for the AI to work");

				//We should rebake the navmesh when the world is rebuilt
				//Comment ou because it take a long time and seems like Unity crash but its not
				//NavMeshBuilder.BuildNavMesh ();
			}
		}


	}
}
