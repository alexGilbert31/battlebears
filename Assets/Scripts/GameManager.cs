﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {


	//This is called the singleton pattern.
	//A singleton is a script with a static reference to the single instance of it

	// ** SINGLETON SETTINGS **
	//private instance to safe check the singleton inside the get method
	private static GameManager _instance;

	//Get method to acces the singleton in all other class
	public static GameManager instance {
		get { 
			//If already create, return the already build object
			if (_instance == null) {
				//This is only true once, when the scene load
				_instance = FindObjectOfType<GameManager> ();
			}

			return _instance;
		}
		
	}

	public Outpost[] outposts;

	public Color[] teamColors;

	//Awake is call before Start and only once
	void Awake () {
		//Get all the flag and return it inside the array
		outposts = FindObjectsOfType<Outpost> ();
	}

	void Start () {
		//Allow to set the active scene, that solve the problem of all the AI spawning
		//inside the MenuScene instead of the GameScene during play time
		SceneManager.SetActiveScene (SceneManager.GetSceneByName ("GameScene"));
	}

	
	// Update is called once per frame
	void Update () {
	
	}
}
