﻿using UnityEngine;
using System.Collections;

public class AIController : BaseUnit {

	private NavMeshAgent agent;

	private enum State {
		Idle,
		MovingToOutPost,
		ChasingEnemy,
		FlyingToOutPost
	}

	private State currentState;
	private Outpost currentOutpost;

	private BaseUnit currentEnemy;
	public float attackRange;
	public float attackInterval;

	public int percentageFlyingVSWalking = 50;

	//this the for shooting at the head ennemies and not his feet
	private Vector3 shootOffset = new Vector3 (0, 1.5f, 0);



	protected override void Start () {
		base.Start ();
		//Get the
		agent = GetComponent<NavMeshAgent> ();
	
	

		//Start the AI system with the Idle state 
		ChangeState (State.Idle);
	}

	void ChangeState (State newState) {
		//Stop all before restarting a new one
		StopAllCoroutines ();
		//Set the current state
		currentState = newState;

		//Call the current function for the currentState
		switch (currentState) {
		case State.Idle:
			StartCoroutine (OnIdle ()); 
			break;

		case State.MovingToOutPost:
			StartCoroutine (OnMovingToOutpost ()); 
			break;
		case State.ChasingEnemy:
			StartCoroutine (OnChasingEnemy ()); 
			break;
		case State.FlyingToOutPost:
			StartCoroutine (OnFlyingToOutpost ()); 
			break;
		}

	
	}

	//Kind of a home made Update method
	IEnumerator OnIdle () {

		while (currentOutpost == null) {
			
			LookForOutpost ();
			//This will pause the execution of this Coroutine for 1 frame
			yield return null;
		
		}
		if (Random.Range (0, 100) < percentageFlyingVSWalking && hasJetpack) {
			ChangeState (State.FlyingToOutPost);
		} else {

			ChangeState (State.MovingToOutPost);
		}

	}


	//Kind of a home made Update method
	IEnumerator OnFlyingToOutpost () {

		//Cannot use that for flying cause agent dosen't handle 3d position...
		agent.SetDestination (currentOutpost.transform.position);


		//we need to do it manually

		Vector3 targetPos = currentOutpost.transform.position;
		Vector3 startpos = transform.position;

		float height = 0.0f;
		bool goingDown = false;

		int randomTop = Random.Range (10, 15);
		int randomBottom = Random.Range (4, randomTop);
		int distanceBeforeGoingDown = randomTop;

		//last value = 10 max and 5 bottom




		//As long as the outpost is not fully captured by my team, stay in this state
		while (!(currentOutpost.captureValue == 1 && currentOutpost.team == this.team)) {

			float distance = Vector3.Distance (transform.position, targetPos);

			//Allow the make a random movement in the air between 15 and 0 on the y axis
			//if teddy is close is location make him go on the floor again
			if (distance < distanceBeforeGoingDown) {
				height -= 0.2f;
				if (height < 0.5f) {
					height = 0.213f;
					particleJet.StopJet ();
				}
			} else {

				if (height > randomTop) {
					height -= 0.1f;
					goingDown = true;
				} else if (height > randomBottom && height < randomTop && goingDown) {
					height -= 0.1f;
					goingDown = true;
	
				} else {
					height += 0.1f;
					//goingUp = true;
					goingDown = false;
				}
			}
			//Apply y value to the teddy
			ApplyForceToFly (height);
			LookForEnemies (true);
		

			//This will pause the execution of this Coroutine for 1 frame
			yield return null;

		}

		//When we have fully captured the outpost, we forget the target
		currentOutpost = null;
		ChangeState (State.Idle);
	}


	private void ApplyForceToFly (float h) {


	
		agent.Stop ();

		transform.position = new Vector3 (transform.position.x, h, transform.position.z);

		particleJet.StartJet ();
		if (h < 0.5f) {
			anim.SetFloat ("VerticalSpeed", agent.velocity.magnitude);
			particleJet.StopJet ();
		} else {
			anim.SetFloat ("VerticalSpeed", 0);
		}
		agent.Resume ();
	}

	//Kind of a home made Update method
	IEnumerator OnMovingToOutpost () {



		//Run one time when we enter the OnMovingToOutpost state
		agent.SetDestination (currentOutpost.transform.position);

		//As long as the outpost is not fully captured by my team, stay in this state
		while (!(currentOutpost.captureValue == 1 && currentOutpost.team == this.team)) {
			//Run each frame

			LookForEnemies (false);


			//This will pause the execution of this Coroutine for 1 frame
			yield return null;

		}

		//When we have fully captured the outpost, we forget the target
		currentOutpost = null;
		ChangeState (State.Idle);
	}

	//Kind of a home made Update method
	IEnumerator OnChasingEnemy () {
		
		float attackTimer = 0.0f;

		while (currentEnemy != null && currentEnemy.health > 0) {

			//if the enemy is out of range OR I can not see him, chase him
			if (Vector3.Distance (transform.position, currentEnemy.transform.position) > attackRange ||
			    !CanSee (currentEnemy.transform, currentEnemy.transform.position + shootOffset)) {
				//transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
				agent.SetDestination (currentEnemy.transform.position);
			
			} 

			//if the enemy is in of range, stop chasing, and attack
			else {
				agent.ResetPath ();
				attackTimer += Time.deltaTime;

				if (attackTimer > attackInterval) {
					//Shoot at the enemy, with the offset to shoot at the chest
					ShootAt (currentEnemy.transform, currentEnemy.transform.position + shootOffset);
					attackTimer = 0;
				}
			}


			//This will pause the execution of this Coroutine for 1 frame
			yield return null;

		}

		currentEnemy = null;
		ChangeState (State.Idle);

	}

	/// <summary>
	/// Assign currentOutpost to a random one
	/// </summary>

	void LookForOutpost () {
		//Get a random outpost
		int r = Random.Range (0, GameManager.instance.outposts.Length);
		currentOutpost = GameManager.instance.outposts [r];
	}


	void LookForEnemies (bool isFlying) {
		
		float attackTimer = 0.0f;
		Collider[] surroundingColliders = Physics.OverlapSphere (transform.position, attackRange);

		foreach (Collider c in surroundingColliders) {
			BaseUnit otherUnit = c.GetComponent<BaseUnit> ();

			//IF the collider is a BaseUnit, if its another team and if it's alive, if I can see it
			if (otherUnit != null && otherUnit.team != this.team && otherUnit.health > 0
			    && CanSee (otherUnit.transform, otherUnit.transform.position + shootOffset)) {
				currentEnemy = otherUnit;
				if (!isFlying) {
					ChangeState (State.ChasingEnemy);
					return;
				} else {
					attackTimer += Time.deltaTime;

					if (attackTimer > attackInterval) {
						//Shoot at the enemy, with the offset to shoot at the chest
						ShootAt (currentEnemy.transform, currentEnemy.transform.position + shootOffset);
						attackTimer = 0;
					}
					return;
						
				}
			}

		}
		



	}

	
	// Update is called once per frame
	void Update () {
		
		if (IsGrounded ()) {
			anim.SetFloat ("VerticalSpeed", agent.velocity.magnitude);
		} else {
			anim.SetFloat ("VerticalSpeed", 0);
		}



	}

	protected override void Die () {
		print ("DieCaller");
		base.Die ();
		//Grab last position
		Vector3 pos = transform.position;
		StopAllCoroutines ();
		agent.Stop ();
		//Put the bear there, to make it fall when kill
		transform.position = pos;
		Destroy (GetComponent<Collider> ());
		Destroy (GetComponentInChildren<Sphere> ().gameObject);
		if (GetComponentInChildren<Jetpack> () != null && GetComponentInChildren<JetpackParticle> () != null) {
			Destroy (GetComponentInChildren<Jetpack> ().gameObject);
			Destroy (GetComponentInChildren<JetpackParticle> ().gameObject);
		}
	}




}
