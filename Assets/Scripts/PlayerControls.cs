﻿using UnityEngine;
using System.Collections;

public class PlayerControls : BaseUnit {

	public float speedPlayer;
	private float realSpeedPlayer;
	public float jumpHeight;



	public float respawnTime = 3.0f;



	private int startHealth;

	public float minCamAngle = -60.0f;
	public float maxCamAngle = 60.0f;

	private float angle = 0;

	private int maxPlayerHeight = 20;


	// Use this for initialization
	protected override void Start () {
		startHealth = health;
		base.Start ();

	
	}

	// Update is called once per frame
	void Update () {
		if (Time.timeScale == 0) {
			return;
		}

		//Get axis of the mouse
		float mouseXInput = Input.GetAxis ("Mouse X");
		float mouseYInput = Input.GetAxis ("Mouse Y");


		/////CLAMP CAMERA TO RESTRICT ROTATION ANGLE////
		//Add the input to the angle
		angle += mouseYInput;
		//restrict that angle to min value and the max value that it can go
		angle = Mathf.Clamp (angle, minCamAngle, maxCamAngle);
		//Force apply this angle to the camera every frame
		Camera.main.transform.parent.localRotation = Quaternion.Euler (angle, 0, 0);
		/////CLAMP CAMERA TO RESTRICT ROTATION ANGLE////


		if (health <= 0) {
			return;
		}

		float horizontalInput = Input.GetAxis ("Horizontal");
		float verticalInput = Input.GetAxis ("Vertical");


		if (Input.GetMouseButton (1) && hasJetpack) {
			
			//GetComponent<Rigidbody> ().AddForce (0, jetPackForce, 0);
			Vector3 velo = new Vector3 (0, jetPackForce, 0);
			rb.velocity = velo;
		} 

		if (IsGrounded ()) {
			//Get axis for moving
			//ANIMATION OF THE BEAR
			anim.SetFloat ("VerticalSpeed", verticalInput);
			anim.SetFloat ("HorizontalSpeed", horizontalInput);

		} else {
			anim.SetFloat ("VerticalSpeed", 0);
			anim.SetFloat ("HorizontalSpeed", 0);
			//ANIMATION OF THE BEAR

		}



	

		/////CLAMP SPEED PLAYER ////
		//For the speed when we go backward
		realSpeedPlayer = speedPlayer;

		if (verticalInput < 0) {
			realSpeedPlayer = Mathf.Clamp (realSpeedPlayer, 0, 2);
		}
		if (horizontalInput != 0) {
			realSpeedPlayer = Mathf.Clamp (realSpeedPlayer, 0, 4);
		}
		/////CLAMP SPEED PLAYER ////

		//Move the bear depening of the input we provide
		Vector3 input = new Vector3 (horizontalInput, 0, verticalInput);

		//Make sure the vector get rotate with the player rotation
		//Without that we will go in the same direction even if we rotate
		Vector3 vel = transform.TransformVector (input) * realSpeedPlayer;
		//Set the Y to the jump
		if (Input.GetKeyDown (KeyCode.Space) && IsGrounded ()) {
			//Animation to jump
			anim.SetTrigger ("Jump");
			//Add the jump height to the velocity of the bear
			vel.y = jumpHeight;

		} else { 
			//If we are not jumping, we set the velocity to the original velocity
			//If we dont do this, the velocity will we reset to 0 every frame (and the object float)
			vel.y = rb.velocity.y;
		}

		//Dont multiply the speed here because then the Y value get multiply every frame
		rb.velocity = vel;

		//Rotate the bear
		transform.Rotate (0, mouseXInput, 0);



		if (Input.GetMouseButtonDown (0)) {
	

			Ray camRay = new Ray (Camera.main.transform.position, Camera.main.transform.forward);
			RaycastHit hitInfo;

			if (Physics.Raycast (camRay, out hitInfo)) {
				print (hitInfo.transform);
				
				//hitInfo.point provide us the position in world space where the ray hit the collider
				if (CanSee (hitInfo.transform, hitInfo.point)) {

					print (hitInfo.transform);
					ShootAt (hitInfo.transform, hitInfo.point);

				}

			}
		}

		if (particleJet != null) {
			if (Input.GetMouseButton (1)) {
				particleJet.StartJet ();
			} else {
				particleJet.StopJet ();
			}
		}

		//Maxium height value for the player
		transform.position = new Vector3 (transform.position.x, Mathf.Clamp (transform.position.y, 0.213f, maxPlayerHeight), transform.position.z);

	

	

	}




	protected override void Die () {
		base.Die ();
		Invoke ("Respawn", respawnTime);
	}


	void Respawn () {
		anim.SetBool ("Death", false);
		health = startHealth;
	}
}
