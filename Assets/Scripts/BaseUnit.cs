﻿using UnityEngine;
using System.Collections;

public class BaseUnit : MonoBehaviour {

	public int team;
	public int health;
	public int attackPower;
	public float viewAngle;

	protected Rigidbody rb;
	protected Animator anim;

	private Eye[] eyes;


	public Laser laserPrefab;
	public Color teamColor;


	public float jetPackForce = 75.0f;
	protected bool hasJetpack = false;
	protected JetpackParticle particleJet;

	protected float raycastDistance = 0.3f;




	// Use this for initialization
	protected virtual void Start () {



		anim = GetComponentInChildren<Animator> ();
		rb = GetComponent<Rigidbody> ();

		teamColor = GameManager.instance.teamColors [team];


		GetComponentInChildren<Sphere> ().gameObject.GetComponent<Renderer> ().material.color = teamColor;

		transform.Find ("Teddy/Teddy_Body").GetComponent<Renderer> ().material.color = teamColor;
		//get CompenentSSSS return a array of all child objects with this script
		eyes = GetComponentsInChildren<Eye> ();

		if (GetComponentInChildren<Jetpack> () != null) {
			hasJetpack = true;
			particleJet = GetComponentInChildren<JetpackParticle> ();
		}
	}

	//Make sure that the camera and the eye can see the same object before shoothing
	protected bool CanSee (Transform hitObject, Vector3 hitPos) {

		foreach (Eye eyeObj in eyes) {
			//The direction from A to B is calculated by using Vector3 dir = B-A
			Vector3 directionFromEye = hitPos - eyeObj.transform.position;

			//The angle between forward and the direction vector of the enemy
			if (Vector3.Angle (transform.forward, directionFromEye) > viewAngle) {
				return false;
			}

			Ray eyeRay = new Ray (eyeObj.transform.position, directionFromEye);


			RaycastHit hitInfo;
			//We raycast towards the postion that the camera hit
			if (Physics.Raycast (eyeRay, out hitInfo)) {

				//If the object is the same as the object that the camera hit
				//This Teddy can see the hitObject
				if (hitInfo.transform == hitObject) {

					return true;
				}
			}
		}
	
		return false;
	}


	protected void ShootAt (Transform hitObject, Vector3 hitPos) {

		BaseUnit unit = hitObject.GetComponent<BaseUnit> ();
		BaseUnit unit2 = hitObject.GetComponentInChildren<BaseUnit> ();
		BaseUnit unit3 = hitObject.GetComponentInParent<BaseUnit> ();

		if (unit != null && unit.team != this.team) {

			//I call OnHit on the Other BAseUnit object
			unit.OnHit (attackPower);
		}






		foreach (Eye eyeObj in eyes) {
			//Create a clone of the laser
			Laser laserClone = Instantiate (laserPrefab);

			//Sart from the eye position to the hitPos
			laserClone.Init (eyeObj.transform.position, hitPos, teamColor);
		}

			


	}

	public void OnHit (int damage) {
		health -= damage;

		if (health <= 0) {
			Die ();
		}
	}

	protected virtual void Die () {
		anim.SetBool ("Death", true);
	
		//Destroy (gameObject);
	}

	protected bool IsGrounded () {
		//Check if the raycast touch something or not
		//Return true if he touch something, return false if not
		return Physics.Raycast (transform.position, Vector3.down, raycastDistance);
	}
}
