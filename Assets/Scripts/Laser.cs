﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {
	
	private float lifeTimeLaser = 0.05f;

	public void Init (Vector3 startPos, Vector3 endPos, Color color) {
		LineRenderer line = GetComponent<LineRenderer> ();
		line.SetPosition (0, startPos);
		line.SetPosition (1, endPos);
		line.material.SetColor ("_TintColor", color);
		Destroy (gameObject, lifeTimeLaser);
	}

}
